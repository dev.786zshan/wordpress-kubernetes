
# Production-Ready Wordpress Setup (Kubernetes based)

**TODO*** Wordpress Kubernetes

## Kubernetes Pod Infrastructure

- **TODO*** (show visual of complete k8s infrastructure)

---

## Wordpress Stack

- Wordpress (php7.3-fpm-alpine)
- MySQL 8.0
- Nginx (latest alpine)
- Redis (latest alpine)
- Traefik (Top-level reverse proxy - default K3s Ingress-Controller)

### Additional Features/Mods (automated upon deployment)

- Removed limits on file upload size (nginx conf)
- IONCube extension integration
- Write permissions for __wp-content__ directory
- Web based Wordpress version update support (FTP-less)
  - Via setting __FS_METHOD__ variable
- Automated Redis integration (modify wp-config.php) - support for [Redis Object Cache](https://wordpress.org/plugins/redis-cache/)
  - Via setting __WP_CACHE__ and __WP_CACHE_KEY_SALT__ variables
- Option to automate site setup site via [WP CLI](https://wp-cli.org/)

### Wordpress Deployment Pre-requisites

- set wordpress & mysql db environment variables in **.env** file
- Kubernetes distribution - k3d (reccomended) and dockerized k3s setup provided below for local setup

### Deployment of resources

```sh
kubectl create secret generic wp-secrets --from-env-file=.env
kubectl apply -f wordpress-volumes.yml
kubectl apply -f wordpress-database.yml
kubectl apply -f wordpress-nginx.yml
kubectl apply -f wordpress-ingress.yml
```

### Automate Wordpress Site setup via CLI (post pod deployment)

Site setup is based on the following environment variables - **VIRTUAL_HOST**, **WP_CLI_ADMIN_USER**, **WP_CLI_ADMIN_PASS**, **WP_CLI_ADMIN_EMAIL**

- **Note: Remember to change these variables when running on server (in prod)**
- Run following script once all deployments are running (site is reachable)

```sh
VIRTUAL_HOST=localhost \
WP_CLI_ADMIN_USER=wpadmin \
WP_CLI_ADMIN_PASS=wppass \
WP_CLI_ADMIN_EMAIL=admin@admin.com &&
kubectl exec -i $(kubectl get pod -l "app=wp" -o jsonpath='{.items[0].metadata.name}') -c wordpress sh <<EOF
wp --allow-root core install --path="/var/www/html/" --url="$VIRTUAL_HOST" --title="Wordpress By Docker" --admin_user=$WP_CLI_ADMIN_USER --admin_password=$WP_CLI_ADMIN_PASS --admin_email=$WP_CLI_ADMIN_EMAIL;
EOF
```

### Cleanup of resources

```sh
kubectl delete secrets wp-secrets
kubectl delete -f wordpress-volumes.yml
kubectl delete -f wordpress-database.yml
kubectl delete -f wordpress-nginx.yml
kubectl delete -f wordpress-ingress.yml
```

---

## K3d setup

We can use [K3D (K3s in Docker)](https://github.com/rancher/k3d) to set up a lightweight fully compatible kubernetes distribution running via docker.  
We export KUBECONFIG environment variable to gain access to kubernetes CLI (kubectl).  

### K3d Pre-requisites

- k3d binary
- docker

Create a cluster, exposing api at port 6550, mapping the ingress port 80 to localhost:80 with 2 worker nodes:

```sh
k3d create --api-port 6550 -v ${PWD}/storage/:/var/lib/rancher/k3s/storage/ -p 80:80 --workers 2
```

- **Note**: K3s PersistentVolumes mounted to host in case we want to persist/reuse pod files - e.g. useful when migrating local work to server

Get the kubeconfig file:

```sh
export KUBECONFIG="$(k3d get-kubeconfig --name='k3s-default')"
```

### Verify K3s node(s) and startup resources (storage class)

```sh
kubectl get nodes
kubectl get pods --all-namespaces
kubectl get storageclass
```

---

## K3s Setup (K3d alternative)

This will setup a [k3s](https://k3s.io/) server and client so you can run production-ready kubernetes within docker.  
We export KUBECONFIG environment variable to gain access to kubernetes CLI (kubectl).  

### K3s Pre-requisites

- set **K3S_TOKEN** environment variables in __docker-compose.yml__ file

```sh
cd k3s
docker-compose up -d
cd ../
export KUBECONFIG=./k3s/kubeconfig.yaml
```

#### Port-forward service to host for local testing (ignores deployed ingress resource)

```sh
kubectl port-forward service/nginx-svc 8000:80
```

**Note**: Use this approach in cases where k3d cannot be installed/used (e.g. container-optimized OS - GCP)

---

## Limitations/Constraints

- Self hosted database (on k8s cluster)
  - Ideally Database should be managed by cloud provider - on a seperate VM

---

## TODO

- Namespace all deployable resources
- Complete description
- Provide infrastructure visual
- Package all resources (yaml files) into a single configurable [helm chart](https://helm.sh/docs/topics/charts/)
